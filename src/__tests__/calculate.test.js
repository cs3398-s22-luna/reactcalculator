import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"2")).toHaveProperty('current', "2");
    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+")).toHaveProperty('current', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                previous: 2,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "4");
        expect(calculate({
                previous: 2,
                current: 21,
                operation: "+"},"=")).toHaveProperty('previous', "23");
    });
});


